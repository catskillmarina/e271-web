Terms of Service for e271.net

1) e271.net is a limited shared service. Some sites are hosted for a small fee
and some are hosted gratis. e271.net reserves the right to remove sites that
may cause service degradation due to traffic or attacks. (In other words, if you
get into an internet battle we may or may not defend your site and may remove
the site to preserve the service for others.) (The same holds for any other high
traffic situation. If you expect high traffic or attacks, talk to us but we 
reserve the right to decline any high profile or controversial sites.)

1a) The costs of hosting high traffic or controversial sites will be born by 
the site owner regardless of any previous agreement.

2) The Legal Jurisdiction for disputes arising due to use of this service is 
Orleans County VT.

3) Users of the service agree to binding arbitration in case of any disputes 
should e271.net wish to use arbitration. 

3b) Person raising the dispute agree to pay any arbitration costs, though e271
may at its discression contribute to paying these costs.

4) Hate speech of any sort that involves e271.net servers in any way is grounds
for immediate removal. Even if you simply mention your site on e271 in any 
internet profile, leaflet or business card this applies.

4a) Hate speech includes but is not limited to any racial slurs, slurs against 
people due to language, housing status, economic status ,national origin, 
slurs against native people, disabled people, transgender or transexual people 
including intentionally misgendering a trans person. 

5) Free sites are hosted as an at will  service to the community and to 
businesses at e271 discession. If your behavior on or off the internet reflects 
badly on e271.net e271.net reserves the right to remove your site immediately.

5a) We reserve the right to charge a fee of $250/hr 4 hour minimum for site
removal.

6) E271.net will take some measures to prevent data loss but you hold e271.net
harmless for any loss of data or service or business. 
(Keep your own good backups)

7) Maximum value for any loss of data or service will be the pro-rated time of
fees already paid your service is unavailable limited to 6 months hosting fees. 
( If your free site is down for 6 months, we owe you the money you paid us for 
hosting your site. If your pay site is down for 3 months we owe you $15 1/2 of 
the $30 6 month fee.).

8) e271.net may in the course of administration access your data, logs and 
connection information.

8a) You agree to remove email from our service as you retrieve it. We do not 
want any email long term storage on our servers. (If you store email on our
servers long term we may attempt to contact you before removing the data.)

8b) You agree that you will use high grade encryption for any email that must
remain private.

8c) You understand that unencrypted email is not secure and may be seen by 
outside parties.

8d) You will not disclose login information for email, ftp, ssh, mysql or any
other service to third parties who are not specifically authorized by e271.net
to access our services. 

9) We reserve the right, retroactively to publish any harrassing or threatening
correspondence. (If you get nasty, expect to have your threats published for 
public amusement, education and entertainment.)

9a) We reserve the right to remove any services from anyone sending us threats
of any sort or harrassing correspondence.

10) You agree not to host any illegal, harrassing or private information (dox)
on our service. Sites that do this will be removed.

10a) You agree not to use e271.net to send unsolicited messages - spam. You 
agree not to use spam to promote a site hosted on e271.net.

10b) Costs for spam cleanup will be born by the sender billed at $250/hr 4 hour
minimum. (In other words, if i have to handle a single email due to spam, you 
owe e271.net $1000)

10c) You agree not to use e271.net services for copyright violations. We will
comply with valid DMCA copyright violation complaints and you will bear the cost
of our work which is $250/hr 4 hr minimum.

11) We reserve the right to enforce or waive these terms with the exception 
of Term 10.

11a) Terms are subject to change without notice. 

11b) You are responsible for knowing these terms and keeping up with any changes
in these terms posted at http://e271.net/tos.txt.

12) If you do not agree with these terms of service or if you do not understand
them, remove your data from our services and do not connect to them or cause 
connections to them by third parties. 

12a) If you do not agree with or understand all these terms of service you 
agree to contact the management to tell them to remove your data and logins.

12b) By using e271.net services in any way, you are agreeing with these terms
of service.

If any of these terms are not enforcible, they are severable.
