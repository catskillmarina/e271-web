Terms of Service for e271.net

1) e271.net is a limited shared service. Some sites are hosted for a small fee
and some are hosted gratis. e271.net reserves the right to remove sites that
may cause service degradation due to traffic or attacks. (In other words, if you
get into an internet battle we may or may not defend your site and may remove
the site to preserve the service for others.) (The same holds for any other high
traffic situation. If you expect high traffic or attacks, talk to us but we 
reserve the right to decline any high profile or controversial sites.) 
High Traffic is defined as traffic that generates a significant load on 
any e271 servers in the sole opinion of e271.com management. 
(for example - more than 30 hits/minute or expected hacking attempts.)

1a) IF AN AGREEMENT IS MADE TO HOST A HIGH TRAFFIC SITE ON A TEMPORARY BASIS, 
THE ADDITIONAL COST WILL BE BORNE BY THE SITE OWNER AT MARKET RATES with market 
rate system administration fees.

2) The Legal Jurisdiction for disputes arising due to use of this service is 
Orleans County VT.

3) Hate speech of any sort that involves e271.net servers in any way is grounds
for immediate removal. 

3a) Hate speech includes but is not limited to any slurs against people due
to race, religion, ethnic/national origin, sex, creed, socioeconomic status,
language, housing status, disabled status, sexual orientation, or gender
identity/expression, in the sole opinion of e271.com management.

3b) Content offensive to the e271.net management is not permitted. We reserve 
the right to remove sites within 8 hours after attempting to notify the site 
owner. (It's our service)

4) Free sites are hosted as an at will service to the community and to 
businesses at e271 discression. We reserve the right to remove sites within
8 hours after attempting to notify the site owner.

4a) We reserve the right to charge market rates for system administration for 
site removal.  

5) E271.net will take some measures to prevent data loss but you hold e271.net
harmless for any loss of data or service or business. 
(Keep your own good backups)

6) Maximum value for any loss of data or service will be the pro-rated time of
fees already paid your service is unavailable limited to 6 months hosting fees. 
( If your free site is down for 6 months, we owe you the money you paid us for 
hosting your site. If your pay site is down for 3 months we owe you 1/2 of 
the total 6 month fee.). This applies to sites on shared servers, hosts managed
by e271.net and any other service.

7) e271.net may in the course of administration access your data, logs and 
connection information. This information is used for normal administration
of e271.net systems. We will comply with any valid legal request for data from 
the authorities.

7a) You agree to remove email from our service as you retrieve it. We do not 
want any email long term storage on our servers. (If you store email on our
servers long term we may attempt to contact you before removing the data.)
Long term storage is storing email for more than 30 days from receipt. If you 
use e271.net email services and expect to be away for more than 1 month 
please contact us to make arrangements to handle your mail.

7b) You agree that you will use high grade encryption for any email that must
remain private. E271 SHALL NOT BE RESPONSIBLE FOR LOSS OF ANY KIND 
DUE TO THIRD PARTY HACKING INTO THE SITE. You understand that unencrypted
email is not secure and may be seen by outside parties.

7d) You will not disclose login information for email, ftp, ssh, mysql or any
other service to third parties who are not specifically authorized by e271.net
to access our services. 

8) We reserve the right, retroactively to publish any harassing or threatening
correspondence. (If you get nasty, expect to have your threats published for 
public amusement, education and entertainment.)

8a) We reserve the right to IMMEDIATELY remove any services from anyone 
sending threats of any sort or harassing correspondence.

9) You agree not to host any illegal, harrassing or the private information 
of other people without the subjects permission (dox) on our service. Sites 
that do this will be removed. 

9a) You agree not to use e271.net to send unsolicited messages - spam. You 
agree not to use spam to promote a site hosted on e271.net.

9b) Costs for spam cleanup will be borne by the sender billed at market rates
for system administration. 

9c) You agree not to use e271.net services for copyright violations. We will
comply with valid DMCA copyright violation complaints and you will bear the cost
of our work which is market rates for system administration.

10) We reserve the right to enforce or waive these terms with the exception 
of Term 9.

10a) You are responsible for knowing these terms and keeping up with any changes
in these terms posted at http://e271.net/tos.txt.

11) If you do not agree with these terms of service or if you do not understand
them, remove your data from our services and do not connect to them or cause 
connections to them by third parties. 

11a) If you do not agree with or understand all these terms of service you 
agree to contact the management for clarification.

11b) By using e271.net services in any way, you are agreeing with these terms
of service.

If any of these terms are FOUND BY A COURT OF COMPETENT JURISDICTION TO 
BE not Enforceable, they are severable AND THE REMAINING TERMS SHALL BE 
FULLY IN EFFECT.
